<?php namespace App\Database\Seeds;
class TiendaSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $disenadores = 
        [
            [
                'id' => 1,
                'apodo' => 'mejorandolopresente',
                'nombre' => 'Mariano Martínez',
            ],
            [
                'id' => 2,
                'apodo' => 'Sintes',
                'nombre' => 'Santiago Soria',
            ],
            [
                'id' => 3,
                'apodo' => 'pacografico',
                'nombre' => 'Pedro Piqueras',
            ],
            [
                'id' => 4,
                'apodo' => 'asacoy',
                'nombre' => 'Arturo Aznar',
            ]
        ];
        $this->db->table('disenadores')->insertBatch($disenadores);
        $articulos = [
            [
                'descripcion' => 'atari',
                'stock' => 5,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'biker revolution',
                'stock' => 3,
                'precio' => 16.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'cebra',
                'stock' => 1,
                'precio' => 16.90,
                'disenador' => 3,
            ],
            [
                'descripcion' => 'ciclo de krebs',
                'stock' => 2,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'clave de fa',
                'stock' => 4,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'cubo rubik',
                'stock' => 3,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'error 404',
                'stock' => 5,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'estrella de la muerte',
                'stock' => 4,
                'precio' => 14.90,
                'disenador' => 4,
            ],
            [
                'descripcion' => 'estrella roja trazos',
                'stock' => 2,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'frente popular de judea',
                'stock' => 4,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'genius',
                'stock' => 3,
                'precio' => 16.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'igor',
                'stock' => 2,
                'precio' => 14.90,
                'disenador' => 3,
            ],
            [
                'descripcion' => 'hermanos marx',
                'stock' => 4,
                'precio' => 17.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'impredible',
                'stock' => 1,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'jaque mate',
                'stock' => 2,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'keep calm and code python',
                'stock' => 7,
                'precio' => 14.90,
                'disenador' => 4,
            ],
            [
                'descripcion' => 'lowcost',
                'stock' => 1,
                'precio' => 14.90,
                'disenador' => 3,
            ],
            [
                'descripcion' => 'magician cofee',
                'stock' => 1,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'men in tia',
                'stock' => 4,
                'precio' => 12.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'mi primera lista',
                'stock' => 1,
                'precio' => 14.90,
                'disenador' => 2,
            ],
            [
                'descripcion' => 'full guitar',
                'stock' => 5,
                'precio' => 14.90,
                'disenador' => 2,
            ],
            [
                'descripcion' => 'nudo celta',
                'stock' => 2,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'numero pi',
                'stock' => 3,
                'precio' => 15.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'regreso al futuro',
                'stock' => 3,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'que haria macgyver',
                'stock' => 0,
                'precio' => 13.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'reservoir muppets',
                'stock' => 3,
                'precio' => 14.90,
                'disenador' => 4,
            ],
            [
                'descripcion' => 'schrdinger-dirac',
                'stock' => 1,
                'precio' => 14.90,
                'disenador' => 3,
            ],
            [
                'descripcion' => 'se me va la pinza',
                'stock' => 1,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'starry school',
                'stock' => 2,
                'precio' => 14.90,
                'disenador' => 2,
            ],
            [
                'descripcion' => 'surf why vintage',
                'stock' => 1,
                'precio' => 17.90,
                'disenador' => 3,
            ],
            [
                'descripcion' => 'the walking dad',
                'stock' => 3,
                'precio' => 14.90,
                'disenador' => 1,
            ],
            [
                'descripcion' => 'totoro and his umbrella',
                'stock' => 1,
                'precio' => 17.90,
                'disenador' => 3,
            ],
            [
                'descripcion' => 'we can do it',
                'stock' => 5,
                'precio' => 17.00,
                'disenador' => 3,
            ],
            [
                'descripcion' => 'lucha feminista',
                'stock' => 9,
                'precio' => 14.34,
                'disenador' => 4,
            ]
        ];
        $this->db->table('articulos')->insertBatch($articulos);
    }
}