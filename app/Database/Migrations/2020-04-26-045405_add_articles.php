<?php namespace App\Database\Migrations;
class AddArticles extends \CodeIgniter\Database\Migration {
public function up()
{
    // Drop table 'disenadores' if it exists
    $this->forge->dropTable('disenadores', true);
    // Table structure for table 'disenadores'
    $this->forge->addField([
        'id' => [
            'type' => 'INT',
            'constraint' => 11,
            'unsigned' => TRUE,
            'auto_increment' => TRUE
    ],
        'apodo' => [
            'type' => 'VARCHAR',
            'constraint' => '24',
    ],
        'nombre' => [
            'type' => 'VARCHAR',
            'constraint' => 60,
    ],
        'created_at' => [
            'type' => 'DATETIME',
            'null' => TRUE,
    ],
        'updated_at' => [
            'type' => 'DATETIME',
            'null' => TRUE,
    ],
        'deleted_at' => [
            'type' => 'DATETIME',
            'null' => TRUE,
        ]
    ]);
    $this->forge->addKey('id', TRUE); //Primary key
    $this->forge->createTable('disenadores');
    // Drop table 'articulos' if it exists
    $this->forge->dropTable('articulos', true);
    // Table structure for table 'articulos'
    $this->forge->addField([
        'id' => [
            'type' => 'INT',
            'constraint' => 11,
            'unsigned' => TRUE,
            'auto_increment' => TRUE
    ],
        'descripcion' => [
            'type' => 'VARCHAR',
            'constraint' => '30',
    ],
        'stock' => [
            'type' => 'INT',
            'constraint' => 4,
            'default' => 0
    ],
        'precio' => [
            'type' => 'DECIMAL',
            'constraint' => '5,2',
            'default' => 14.90
    ],
        'disenador' => [
            'type' => 'INT',
            'unsigned' => TRUE,
            'constraint' => 11,
            'default' => 1
    ],
        'created_at' => [
            'type' => 'DATETIME',
            'null' => TRUE,
    ],
        'updated_at' => [
            'type' => 'DATETIME',
            'null' => TRUE,
    ],
        'deleted_at' => [
            'type' => 'DATETIME',
            'null' => TRUE,
        ]
    ]);
    $this->forge->addKey('id', TRUE); //Primary key
    $this->forge->addKey('disenador', FALSE, FALSE); //Foraign key
    //Adding Foreign Keys
$this->forge->addForeignKey('disenador','disenadores','id','RESTRICT','CASCADE');    
$this->forge->createTable('articulos');
}


public function down()
{
    $this->forge->dropTable('articulos', true);
    $this->forge->dropTable('disenadores', true);
}

}
