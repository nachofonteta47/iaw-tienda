<?php
return [
// form_alta
        'newArticle' => 'New Article',
        'labelName' => 'Name',
        'labelDesigner' => 'Designer',
        'labelStock' => 'Stock',
        'labelPrice' => 'Price',
        'labelSave' => 'Save',
        'messageName' => 'T-shirt short name',
        'messageStock' => 'Number of shirts in stock',
        'messagePrice' => 'Unit selling price',
        'placeholderName' => 'You have to fill it',
        'messageImage' => 'Select the image',
// menú
];
