<?php
return [
// form_alta
        'newArticle' => 'Nuevo Artículo',
        'labelName' => 'Nombre',
        'labelDesigner' => 'Diseñador',
        'labelStock' => 'Stock',
        'labelPrice' => 'Precio',
        'labelSave' => 'Guardar',
        'messageName' => 'Nombre corto de la camista',
        'messageStock' => 'Número de camisetas en stock',
        'messagePrice' => 'Precio unitario de venta',
        'placeholderName' => 'Hay que rellenarlo',
        'messageImage' => 'Selecciona la imagen',
// menú
];
