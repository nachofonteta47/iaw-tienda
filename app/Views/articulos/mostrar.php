<?php
    $auth = new \IonAuth\Libraries\IonAuth();
?>
<?= $this->extend('comun/layout') ?>
<?= $this->section('contenido') ?>
<a href="<?= site_url('tienda/nuevo') ?>">
    <span class="fas fa-plus"></span>
</a>
<?php if (! empty($articulos) && is_array($articulos)) : ?>

<a href="<?= site_url ('tienda/carro')?>" class="fas fa-cart-arrow-down">Mi Carrito</a>
<div class ="row">
<?php foreach ($articulos as $articulo): ?>
    

<div class="col-3">
    <div class="card text-center h-100">
        <img src="<?php echo base_url("assets/images/camisetas/".str_replace(" ","_",$articulo->descripcion).".jpg");?>" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title"><?= $articulo->descripcion ?></h5>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-6">
                    <?= $articulo->precio ?> €
                </div>
                <div class="col-6">
                    <nav class="navbar navbar-light bg-light">
                        <a href="<?= site_url('tienda/comprar/'.$articulo->id) ?>" title="Añadir al carrito de la compra" >
                            <span class="float-right fas fa-cart-plus"></span>
                        </a>
                        <?php if ($auth->loggedIn() AND $auth->isAdmin()):?>
                        <a href="<?= site_url('tienda/edita/'.$articulo->id) ?>" title="Editar el artículo">
                            <span class="float-right fas fa-pencil-alt"></span>
                        </a>
                        <a href="<?= site_url('tienda/borrar/'.$articulo->id) ?>" onclick="return confirm('¿Estar seguro de borrar el artículo?');" title="Borrar el artículo">
                            <span class="float-right text-danger fas fa-trash-alt"></span>
                        </a>
                        <?php endif; ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
 
    <?php endforeach; ?>
</div>       
    <?= $pager->links('default','bootstrap') ?>
<?php else : ?>
        <h3>No hay artículos</h3>
        <p>No disponemos de stock en este momento.</p>
<?php endif ?>

<?= $this->endSection() ?>