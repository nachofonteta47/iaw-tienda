<?= $this->extend('comun/layout') ?>
<?= $this->section('contenido') ?>
<?php if (!empty(\Config\Services::validation()->getErrors())):?>
<div class="alert alert-danger" role="alert">
 <?= \Config\Services::validation()->listErrors(); ?>
</div>
<?php endif; ?>
<?php $session = \Config\Services::session(); ?>
<?php if ($session->has('mensaje_exito')):?>
<div class="alert alert-success" role="alert">
   <?= $session->getFlashdata('mensaje_exito') ?>
</div>
<?php endif; ?>


   <?= form_open_multipart(site_url('tienda/nuevo')) ?> 
      <div class="row">
                  


            <div class="col-8">      
               <div class="form-group row">
               <!--<label for="descripcion" class="col-2 col-form-label">Nombre</label>-->
                <?= form_label('Nombre','descripcion',['class'=>'col-2 col-form-label']) ?>
                  <div class="col-8">
                     <!--<input id="descripcion" name="descripcion" placeholder="Hay que rellenarlo" type="text" required="required" class="formcontrol">-->
                     <?= form_input('descripcion',set_value('descripcion'),['placeholder'=>"Hay que rellenarlo", 'required'=>"required",'class'=>"form-control" ])?>
                     <span id="descripcionHelpBlock" class="form-text text-muted">Nombre corto de la camista</span>
                  </div>
               </div>
               <div class="form-group row">
               <?= form_label('Diseñador','disenador',['class'=>'col-2 col-form-label']) ?>
                  <div class="col-8">
                     <?= form_dropdown('disenador', $options,set_value('disenador'),['class'=>"form-control" ])?>
                     <span id="disenadorHelpBlock" class="form-text text-muted">Diseñador</span>
                  </div>
               </div>            
               <div class="form-group row">
                  <?= form_label('Stock','stock',['class'=>'col-2 col-form-label']) ?>
                  <div class="col-8">
                     <?= form_input('stock',set_value('stock'),['required'=>"required", 'class'=>"form-control" ])?>
                     <span id="stockHelpBlock" class="form-text text-muted">Número de camisetas en stock</span>
                  </div>
               </div>
               <div class="form-group row">
                  <?= form_label('Precio','precio',['class'=>'col-2 col-form-label']) ?>
                  <div class="col-8">
                     <?= form_input('precio',set_value('precio'),['required'=>"required", 'class'=>"form-control" ])?>
                     <span id="precioHelpBlock" class="form-text text-muted">Precio unitario de venta</span>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="offset-2 col-10">
                     <?= form_submit('boton','Guardar',['class'=>"btn btn-primary"]) ?>
                     <!--<button name="submit" type="submit" class="btn btnprimary">Guardar</button>-->
                  </div>
               </div>
            </div>
         <div class='col-4'>
                     <div class="custom-file">
                        <?= form_upload(['name'=>'imagen','id'=>'imagen','class'=>'custom-file-control','onchange'=>'readURL(this);','style'=>'display:none;']) ?>
                        <?= form_label('Selecciona la imagen','imagen',['class'=>'custom-file-label']) ?>
                     </div>
                        <div class="col-12">
                           <img id="visor" class="" width="100%">
                        </div>

         </div>
      </div>
   <?= form_close() ?>

<?= $this->endSection() ?>
<?= $this->section('titulo') ?>
<?= $titulo?>
<?= $this->endSection() ?>