
<nav class="navbar navbar-expand-lg fnavbar-dark bg-dark">
    <a class="navbar-brand" href="#">Camisetalandia</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" ariacontrols="navbarSupportedContent" aria-expanded="false" arialabel="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sronly">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" ariahaspopup="true" aria-expanded="false">
                    <?=lang('Article.language')?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?=site_url('tienda/setLanguage/es')?>"><?=lang('Article.spanish')?>Español</a>
                    <a class="dropdown-item" href="<?=site_url('tienda/setLanguage/en')?>"><?=lang('Article.english')?>Inglés</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Disabled</a>
            </li>
        <?php if ($auth->loggedIn()):?>
            <?php $user = $auth->user()->row();?>
            <li class="nav-item dropdown " style="min-width:240px;" >
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <img src="<?= base_url('assets/images/users/'.$user->id.'.jpg')?> class="rounded-circle" alt="User Image" width="20px;">
                    <span class="hidden-xs"><?= $user->first_name.' '.$user->last_name?></span>
                </a>
                <ul class="dropdown-menu text-center bg-secondary" style="min-width:240px;">
                    <!-- User image -->
                    <li class="user-header ">
                    <img src="<?= base_url('assets/images/users/'.$user->id.'.jpg')?>" class="rounded-circle" alt="User Image" width="160px;">
                        <p>
                            <?= $user->first_name.' '.$user->last_name?>
                        </p>
                        <p>
                        <?php //echo var_dump($user); ?>
                        <small><?php echo $auth->inGroup('members') ? 'Cliente ' : 'Administrador ';?> desde <?= date('j \d\e F',$user->created_on)?></small>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="bg-light" style="min-height:50px;padding:10px;">
                        <a href="#" class="btn btn-primary btn-sm float-left">Perfil</a>
                        <a href="<?= site_url('auth/logout')?>" class="btn btn-primary btn-sm float-right">Desconectar</a>
                    </li>
                </ul>
            </li>
        <?php else: ?>
            <li class="nav-item">
                <a class="nav-link btn btn-danger text-white" href="<?= site_url('auth/login')?>">Entrar</a>
            </li>
        <?php endif;?>
        </ul>


        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>