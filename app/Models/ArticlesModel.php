<?php namespace App\Models;
use CodeIgniter\Model;
class ArticlesModel extends Model {
 protected $table = 'articulos';
 protected $primaryKey = 'id';
 protected $returnType = 'object';
 protected $useSoftDeletes = true;
 protected $allowedFields = ['descripcion', 'disenador', 'stock', 'precio'];
 protected $useTimestamps = true;
 protected $createdField = 'created_at';
 protected $updatedField = 'updated_at';
 protected $deletedField = 'deleted_at';
 protected $validationRules = [
    'descripcion' => 'required|alpha_numeric_space|min_length[3]',
    'disenador' => 'required',
    'stock' => 'required|is_natural',
    'precio' => 'required|decimal|greater_than[10]',
];
    
 protected $validationMessages = [];
 protected $skipValidation = false;

   
}

