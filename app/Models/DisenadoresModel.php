<?php namespace App\Models;
use CodeIgniter\Model;
class DisenadoresModel extends Model 
{
    protected $table = 'disenadores';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    protected $useSoftDeletes = true;
    protected $allowedFields = ['apodo', 'nombre'];
    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $validationRules = [
    'apodo' => 'required|min_length[3]',
    'nombre' => 'required|min_length[10]',
    ];
    protected $validationMessages = [];
    protected $skipValidation = false;
    public function getDisenadores()
    {
        $disenadores = $this->select("id")
            ->select("CONCAT(apodo,' - ', nombre) as options", FALSE)
            ->get() //SELECT id, CONCAT(apodo,' - ', nombre) as options FROM disenadores
            ->getResult(); //devuelve un array de objetos, cada objeto es un registro
        $options = [];
        foreach($disenadores as $undisenador)
        {
            $options[$undisenador->id] = $undisenador->options;
        }
        return $options;
    }
}